scalapack = True
fftw = True
libraries += ['fftw3']

scalapack = True
libraries += ['scalapack-openmpi']

elpa = True
include_dirs += ['/usr/include/elpa']
libraries += ['elpa']
